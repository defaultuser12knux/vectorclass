#include <iostream>

class Vector
{
private:

	int a = 3;
	int b = 2;
	int sol = (a * a) + (b * b);
	float VectMod = sqrt(sol);

public:

	int GetA()
	{
		return a;
	};

	int GetB()
	{
		return b;
	};

	float VectorModule()
	{
		return VectMod;
	}
};

void main()
{
	Vector temp;
	std::cout << temp.VectorModule() << std::endl;
}